# Nincekon

Mon portfolio pour présenter mes projets et faire certains tests.

![bannière](.ressources/ban.png)

## 1. <a name='Tabledesmatires'></a> Table des matières

<!-- vscode-markdown-toc -->

- 1. [ Table des matières](#Tabledesmatires)
- 2. [Pourquoi ce dépôt ?](#Pourquoicedpt)
- 3. [Que propose ce dépôt ?](#Queproposecedpt)
  - 3.1. [Les labels](#Leslabels)
  - 3.2. [Le Board](#LeBoard)
  - 3.3. [Les branches](#Lesbranches)
  - 3.4. [Modèles pour les issues et les merges requests](#Modlespourlesissuesetlesmergesrequests)
- 4. [Comment utiliser ce dépôt ?](#Commentutilisercedpt)
- 5. [About Laravel 11](#Laravel11)
- 6. [Licence](#Licence)
- 7. [Auteur](#Auteur)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## 2. <a name='Pourquoicedpt'></a>Pourquoi ce dépôt ?

Ce dépot à pour objectif de fournir un outil de démarrage rapide pour organiser et gérer un nouveau projet avec GitLab.

Partant du constat que le démarrage d'un projet est un processus long et complexe, j'ai voulu fournir un outil simple et efficace pour lancer un projet avec GitLab et ses outils !

## 3. <a name='Queproposecedpt'></a>Que propose ce dépôt ?

Ce dépôt fournit un ensemble de modèles, de fichiers et de paramétrages pour vous faciliter le démarrage d'un projet avec GitLab, vous y retrouverez les éléments suivants :

- Ce fichier README.md
- Des modèles pour les issues et les merges requests
- Une collection de labels
- Un modèle de Board
- 3 Branches :
  - Main
  - Pré-Production
  - Production

### 3.1. <a name='Leslabels'></a>Les labels

Les labels sont des éléments qui sont associés à des issues et merges requests et qui permettent de les classer, le organiser et de les identifier simplement.

![label](.ressources/label.gif)

### 3.2. <a name='LeBoard'></a>Le Board

Le Board est l'outil central de GitLab pour organiser et gérer votre projet.

Il va vous permettre de visualiser les différentes tâches que vous avez à accomplir, et de suivre leur progression.

La structure de ce board adopte l'approche [Scrumban](https://asana.com/fr/resources/scrumban).

![board](.ressources/board.gif)

### 3.3. <a name='Lesbranches'></a>Les branches

Les trois branches proposées dans ce template permettent de gérer de manière simple l'état du déploiement de votre projet.

Elles sont particulièrement utiles si vous leurs associez des pipelines GitLab CI pour automatiser le déploiement de votre projet en fonction des branches.

Ce modèle s'inspire librement de l'approche [GitLab Flow](https://www.youtube.com/watch?v=ZJuUz5jWb44).

![](.ressources/branche.png)

### 3.4. <a name='Modlespourlesissuesetlesmergesrequests'></a>Modèles pour les issues et les merges requests

Ce template propose des modèles pour les issues et les merges requests afin de simplifier et standardiser leur utilisation par les équipes du projet.

![board](.ressources/issue.gif)

![board](.ressources/mr.gif)

## 4. <a name='Commentutilisercedpt'></a>Comment utiliser ce dépôt ?

> Vous pouvez utiliser ce dépôt comme **source d'inspiration** pour votre animer votre projet avec GitLab simplement en copiant et adaptant les idées que vous trouverez pertinentes dans votre contexte.

> **Vous pouvez également télécharger l'export du dépôt pour l'importer avec tous les éléments dèja présents (labels, issues, merges requests, board, branches, ...)**
>
> 1. [Télécharger l'export du dépôt](.ressources/export.tar.gz)
> 2. [Importer l'export dans GitLab](https://docs.gitlab.com/ee/user/project/settings/import_export.html#import-a-project-and-its-data)
> 3. Amusez-vous bien !

## 5. <a name='Laravel11'></a> About Laravel 11

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

### Description

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/11.x/routing).
- [Powerful dependency injection container](https://laravel.com/docs/11.x/container).
- Multiple back-ends for [session](https://laravel.com/docs/11.x/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/11.x/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/11.x/migrations).
- [Robust background job processing](https://laravel.com/docs/11.x/queues).
- [Real-time event broadcasting](https://laravel.com/docs/11.x/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

### Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs/11.x/) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

### Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the [Laravel Partners program](https://partners.laravel.com).

#### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[WebReinvent](https://webreinvent.com/)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[DevSquad](https://devsquad.com/hire-laravel-developers)**
- **[Jump24](https://jump24.co.uk)**
- **[Redberry](https://redberry.international/laravel/)**
- **[Active Logic](https://activelogic.com)**
- **[byte5](https://byte5.de)**
- **[OP.GG](https://op.gg)**

### Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

### Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

### Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## 6. <a name='Licence'></a>Licence

Ce dépôt est sous licence [MIT](LICENSE)

## 7. <a name='Auteur'></a>Auteur

&copy; Nincekon - Seeker225 with [YoanDev](https://yoandev.co)
