<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProjectRequest;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        $projects = Project::get();
        return response()->json($projects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProjectRequest $request) : JsonResponse
    {
        $project = new Project;

        $project->title = $request->title;
        $project->description = $request->description;

        $project->save();

        return response()->json($project);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $id) : JsonResponse
    {
        $project = Project::find($id);

        return response()->json($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, string $id) : JsonResponse
    {
        $project = Project::find($id);

        $project->title = $request->title;
        $project->description = $request->description;

        $project->save();

        return response()->json($project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $id) : JsonResponse
    {
        Project::destroy($id);

        return response()->json(['message' => 'deleted']);
    }
}
